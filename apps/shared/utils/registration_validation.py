from abc import ABC, abstractmethod

from users.models import User


class StaticRegistrationErrorMessages:
    # Username
    WRONG_SYMBOL_USERNAME = 'You can\'t use symbol \'@\' in your username field'
    LENGTH_USERNAME = 'The length of your username must contain at least 3 letters.'
    USERNAME_EXISTS = 'This username is already taken'
    # First Name

    @staticmethod
    def get_name_error(name):
        return "Please provide your real %s. This field must have only english letters." % name

    @property
    def first_name_error(self):
        return self.get_name_error('first name')

    @property
    def last_name_error(self):
        return self.get_name_error('last name')

    @property
    def middle_name_error(self):
        return self.get_name_error('middle name')


static_errors = StaticRegistrationErrorMessages()


class Handle(ABC):
    @abstractmethod
    def validate(self):
        """Validate given input"""


class BaseHandle(Handle):
    def __init__(self, text):
        self.text = text
        self.dict_errors = {}

    def validate(self):
        """Some validation goes here..."""


class HandleUsername(BaseHandle):
    def validate(self):
        if str(self.text).find('@') != -1:
            message = static_errors.WRONG_SYMBOL_USERNAME
            self.dict_errors['wrong-symbol'] = [message]

        if len(self.text) < 3:
            message = static_errors.LENGTH_USERNAME
            self.dict_errors['username-length'] = [message]

        if User.objects.filter(username=self.text).exists():
            message = static_errors.USERNAME_EXISTS
            self.dict_errors['username'] = [message]

        return self.dict_errors


class HandleFirstName(BaseHandle):
    def validate(self):
        if len(self.text) <= 1 or not str(self.text).isalpha():
            message = static_errors.first_name_error
            self.dict_errors['fname-error'] = [message]
        return self.dict_errors


class HandleLastName(BaseHandle):
    def validate(self):
        if len(self.text) <= 1 or not self.text.isalpha():
            message = static_errors.last_name_error
            self.dict_errors['lname-error'] = [message]
        return self.dict_errors


class HandleMiddleName(BaseHandle):
    def validate(self):
        if len(self.text) <= 1 or not self.text.isalpha():
            message = static_errors.middle_name_error
            self.dict_errors['mname-error'] = [message]
        return self.dict_errors
