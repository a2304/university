from django.urls import path, include

from rooms.views import RoomViewSet


urlpatterns = [
    path('room', RoomViewSet.as_view({'get': 'list', 'post': 'create'}))
]
