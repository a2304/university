from django.db import models


# Create your models here.
class Room(models.Model):
    title = models.CharField(max_length=4, unique=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.title
