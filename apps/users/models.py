from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.
class User(AbstractUser):
    full_name = models.CharField(max_length=50)
    username = models.CharField(max_length=50, unique=True)
    email = models.EmailField(max_length=50, unique=True)
    password = models.CharField(max_length=50)
    groups = models.ManyToManyField('schedule.Group', blank=True, related_name='users')

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.username
