from django.urls import path
from .views import RegisterView, LoginView, LogoutView, UserView
from users.views import UserViewSet

urlpatterns = [
    # path('register', RegisterView.as_view()),
    # path('login', LoginView.as_view()),
    # path('logout', LogoutView.as_view()),
    path('user', UserViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('user/<int:pk>', UserView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
]
