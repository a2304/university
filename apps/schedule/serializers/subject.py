from rest_framework.serializers import ModelSerializer
from schedule.models.subject import Subject


class SubjectSerializer(ModelSerializer):
    class Meta:
        model = Subject
        fields = '__all__'

    read_only_fields = ['id']
