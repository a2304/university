from rest_framework import serializers

from schedule.models.schedule import Schedule


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = [
            'id',
            'day',
            'time',
            'group',
            'teacher',
            'subject',
        ]

    read_only_fields = ['id']
