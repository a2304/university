from django.shortcuts import render
from rest_framework.serializers import ModelSerializer

from schedule.models.group import Group

# Create your views here.


class GroupSerializer(ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'

    read_only_fields = ['id']
