from django.db import models


class Subject(models.Model):
    title = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.title

