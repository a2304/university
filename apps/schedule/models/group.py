from django.db import models

COURSE_YEAR = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
)


class Group(models.Model):
    title = models.CharField(max_length=16, unique=True)
    course_year = models.CharField(max_length=1, choices=COURSE_YEAR)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return str(self.title)
