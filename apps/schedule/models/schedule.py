from django.db import models

# Create your models here.
from .group import Group
from .subject import Subject


DAYS = (
    ('monday', 'Понедельник'),
    ('tuesday', 'Вторник'),
    ('wednesday', 'Среда'),
    ('thursday', 'Четверг'),
    ('friday', 'Пятница'),
    ('saturday', 'Суббота'),
)

TIMES = (
    ('1', '8:30 - 9:50'),
    ('2', '10:00 - 11:20'),
    ('3', '11:30 - 12:50'),
    ('4', '13:30 - 14:50'),
    ('5', '15:00 - 16:20'),
    ('6', '16:30 - 17:50'),
)


class Schedule(models.Model):
    day = models.CharField(max_length=20, choices=DAYS, null=False, blank=False)
    time = models.CharField(max_length=20, choices=TIMES, null=False, blank=False)
    subject = models.ManyToManyField(Subject)
    group = models.ManyToManyField(Group)
    teacher = models.ManyToManyField('users.User')

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.day + ' ' + self.time + '-class'
