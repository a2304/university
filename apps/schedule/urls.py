from django.urls import include, path

from schedule.views.group import GroupViewSet
from schedule.views.schedule import ScheduleViewSet
from schedule.views.subject import SubjectViewSet
from shared.rest_framework.router import OptionalSlashRouter

router = OptionalSlashRouter()

router.register('group', GroupViewSet, 'group')
router.register('subject', SubjectViewSet, 'subject')
router.register('schedule', ScheduleViewSet, 'schedule')


urlpatterns = [
    path('', include(router.urls))
]
