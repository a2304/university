from django.contrib import admin

# Register your models here.
from schedule.models.group import Group
from schedule.models.schedule import Schedule
from schedule.models.subject import Subject

admin.site.register(Group)
admin.site.register(Subject)
admin.site.register(Schedule)
