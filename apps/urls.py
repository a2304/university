from django.urls import path, include

from rest_framework.authtoken import views

urlpatterns = [
    path('users/', include(('users.urls', 'users'), 'users')),
    path('schedule/', include(('schedule.urls', 'schedule'), 'schedule')),
    path('rooms/', include(('rooms.urls', 'rooms'), 'rooms')),
    # path('api-token-auth', views.obtain_auth_token)

]
